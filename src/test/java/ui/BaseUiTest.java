package ui;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utility.Driver;
import utility.PlatformName;
import utility.SystemPropertyUtil;

import java.util.LinkedList;
import java.util.Queue;

import static utility.SystemPropertyUtil.getPlatform;
import static utility.SystemPropertyUtil.setDefaultValueForAppiumServiceUrl;

public class BaseUiTest {
    protected static Queue<WebDriver> queueToClose;
    protected static AppiumDriverLocalService appiumDriverLocalService;

    @BeforeSuite(alwaysRun = true)
    public void setBaseProperties() {
        SystemPropertyUtil.loadAllPropsFromFiles();
        queueToClose = new LinkedList<>();
        if (getPlatform().equalsIgnoreCase(PlatformName.M_WEB)) {
            appiumDriverLocalService = new AppiumServiceBuilder()
                    .withEnvironment(System.getenv())
                    .withArgument(GeneralServerFlag.BASEPATH, "/wd/hub/")
                    .withIPAddress("127.0.0.1")
                    .usingAnyFreePort()
                    .withArgument(() -> "--allow-insecure", "chromedriver_autodownload")
                    .build();

            appiumDriverLocalService.start();

            setDefaultValueForAppiumServiceUrl(appiumDriverLocalService.getUrl().toString());
        }
    }

    @AfterSuite(alwaysRun = true)
    public void closeAllDrivers() {
        queueToClose.forEach(WebDriver::quit);
        if (getPlatform().equalsIgnoreCase(PlatformName.M_WEB)) {
            appiumDriverLocalService.stop();
            appiumDriverLocalService.close();
        }
    }

    //todo test if this can be replace by getDriver().quit() in after test
    public WebDriver startDriver() {
        WebDriver driver = new Driver().startNewDriver();
        queueToClose.add(driver);
        return driver;
    }
}