package api;

import io.restassured.RestAssured;
import utility.SystemPropertyUtil;

public class BaseApiTest {

    public BaseApiTest() {
        RestAssured.baseURI = SystemPropertyUtil.getBaseApiUrl();
    }
}
