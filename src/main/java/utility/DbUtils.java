package utility;

import org.testng.Reporter;

import java.sql.*;
//TODO refactor
public class DbUtils {

    private static ResultSet executeQuery(String query) {
        Statement stmt = null;
        String DB_URL = SystemPropertyUtil.getDbUrl();
        String DB_USER = SystemPropertyUtil.getDbUser();
        String DB_PASSWORD = SystemPropertyUtil.getDbPassword();
        ResultSet result = null;

        try {
            String dbClass = "org.postgresql.Driver";
            Class.forName(dbClass).newInstance();
            Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            stmt = con.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (Exception e) {
            CustomAssert.failTest("Error while connecting to DB" + e.getMessage());
        }

        try {
            Reporter.log("Executing query: " + query);
            result = stmt.executeQuery(query);
        } catch (SQLException e) {
            CustomAssert.failTest(String.format("Error while executing select query '%s': ", query) + e.getMessage());
        }

        return result;
    }

    private static int executeUpdate(String query) {
        Statement stmt = null;
        String DB_URL = SystemPropertyUtil.getDbUrl();
        String DB_USER = SystemPropertyUtil.getDbUser();
        String DB_PASSWORD = SystemPropertyUtil.getDbPassword();
        int returnValue = -1;
        try {
            String dbClass = "org.postgresql.Driver";
            Class.forName(dbClass).newInstance();
            Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            stmt = con.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (Exception e) {
            CustomAssert.failTest("Error while connecting to DB" + e.getMessage());
        }

        try {
            Reporter.log("Executing query: " + query);
            returnValue = stmt.executeUpdate(query);
        } catch (SQLException e) {
            CustomAssert.failTest(String.format("Error while executing update query '%s': ", query) + e.getMessage());
        }
        return returnValue;
    }

    private static void deleteQuery(String table, String whereClause) {
        String query = String.format("DELETE FROM %s WHERE %s", table, whereClause);
        executeUpdate(query);
    }

}
