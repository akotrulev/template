package utility;

import api.CustomGson;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chromium.ChromiumNetworkConditions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.v101.network.Network;
import org.openqa.selenium.devtools.v101.network.model.Request;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import ui.pojo.RequestPojo;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static utility.BrowserName.*;

public class DevToolUtil {
    private List<Request> requestList;
    private DevTools devTools;
    private WebDriver driver;

    public DevToolUtil(WebDriver driver) {
        this.driver = driver;
        String driverType = SystemPropertyUtil.getDriver().toLowerCase();
        switch (driverType.toLowerCase()) {
            case CHROME:
                devTools = ((ChromeDriver) driver).getDevTools();
                break;
            case FIREFOX:
                devTools = ((FirefoxDriver) driver).getDevTools();
                break;
            case EDGE:
                devTools = ((EdgeDriver) driver).getDevTools();
                break;
            default:
                CustomAssert.failTest("Browser does not support dev tools");
        }
        devTools.createSessionIfThereIsNotOne();
    }

    public void registerListener(String partialUrl) {
        devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
        requestList = new ArrayList<>();
        devTools.addListener(Network.requestWillBeSent(), requestWillBeSent -> {
            if (requestWillBeSent.getRequest().getUrl().contains(partialUrl)) {
                requestList.add(requestWillBeSent.getRequest());
            }
        });
    }

    public RequestPojo filterRequest(String fieldValue) {
        // Create a copy of the list to avoid ConcurrentModificationException
        List<Request> copyList = new ArrayList<>(requestList);
        LocalTime timestamp = LocalTime.now();
        AtomicBoolean isFound = new AtomicBoolean(false);
        AtomicReference<RequestPojo> atomicReference = new AtomicReference<>(new RequestPojo());
        while (timestamp.plusSeconds(30).isAfter(LocalTime.now())) {
            // Check if there is a difference in the size of the original and copy lists, if yes - replace the copy
            if (copyList.size() < requestList.size()) {
                copyList = new ArrayList<>(requestList);
            }
            if (!copyList.isEmpty()) {
                copyList.forEach(request -> {
                    RequestPojo segmentEventRequestPojo = CustomGson.GSON.fromJson(request.getPostData().get(), RequestPojo.class);
                    if (segmentEventRequestPojo.getField().equals(fieldValue)) {
                        isFound.set(true);
                        atomicReference.set(segmentEventRequestPojo);
                    }
                });
            }
            if (isFound.get()) {
                return atomicReference.get();
            }
        }
        if (atomicReference.get().getField() == null) {
            CustomAssert.failTest(String.format("Request with field '%s' not found.", fieldValue));
        }
        return null;
    }

    public void setInternetSpeed(ChromiumNetworkConditions internetSpeed) {
        Reporter.log(String.valueOf(internetSpeed.getDownloadThroughput()));
        ((ChromeDriver) driver).setNetworkConditions(internetSpeed);
    }
}
