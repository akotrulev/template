package utility;

import com.aventstack.extentreports.service.ExtentTestManager;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.IAlterSuiteListener;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.List;

public class Listener extends ExtentITestListenerClassAdapter implements IAlterSuiteListener {
    @Override
    public void alter(List<XmlSuite> suites) {
        XmlSuite suite = suites.get(0);
        List<String> include = SystemPropertyUtil.getIncludeGroups();
        List<String> exclude = SystemPropertyUtil.getExcludeGroups();
        if (!include.isEmpty()) {
            include.forEach(suite::addIncludedGroup);
        }
        if (!exclude.isEmpty()) {
            exclude.forEach(suite::addExcludedGroup);
        }
    }

    public void takeScreenshot(String pathToFile) {
        TakesScreenshot screenshot = (TakesScreenshot) Driver.getDriver();
        File file = screenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file, new File(pathToFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void onTestFailure(ITestResult iTestResult) {
        String name = "/test-output/screenshots/" + iTestResult.getName() + LocalTime.now().toString() + ".png";
        String path = System.getProperty("user.dir") + name;
        takeScreenshot(path);
        try {
            ExtentTestManager
                    .getTest(iTestResult)
                    .addScreenCaptureFromPath(".." + name)
                    .fail(iTestResult.getThrowable());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
