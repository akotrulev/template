package utility;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class SystemPropertyUtil {
    public final static String BASE_UI_URL = "baseUiUrl";
    public final static String BASE_API_URL = "baseApiUrl";
    public final static String DRIVER = "driver";
    public final static String PLATFORM = "platform";
    public final static String APPIUM_SERVICE_URL = "appiumServiceUri";
    public final static String TIMEOUT = "timeout";
    public final static String DB_URL = "db.url";
    public final static String DB_USER = "db.user";
    public final static String DB_PASSWORD = "db.password";
    public final static String INCLUDE_GROUPS = "includeGroups";
    public final static String EXCLUDE_GROUPS = "excludeGroups";

    public static void loadAllPropsFromFiles() {
        CustomFileReader customFileReader = new CustomFileReader();
        Properties systemProps = customFileReader.readPropFile("system.properties");
        if (!systemProps.isEmpty()) {
            setDefaultValueForSystemProperty(BASE_UI_URL, systemProps);
            setDefaultValueForSystemProperty(BASE_API_URL, systemProps);
            setDefaultValueForSystemProperty(DRIVER, systemProps);
            setDefaultValueForSystemProperty(PLATFORM, systemProps);
            setDefaultValueForSystemProperty(TIMEOUT, systemProps);
            setDefaultValueForSystemProperty(DB_URL, systemProps);
            setDefaultValueForSystemProperty(DB_PASSWORD, systemProps);
            setDefaultValueForSystemProperty(DB_USER, systemProps);
        }
    }

    /**
     * @param propertyName Name of the system parameter
     * @param value        the default value that should be set for that property if it's not already set
     */
    private static void setDefaultValueForSystemProperty(String propertyName, String value) {
        if (System.getProperty(propertyName) == null) {
            System.setProperty(propertyName, value);
        }
    }

    public static void setDefaultValueForPlatform(String platform) {
        setDefaultValueForSystemProperty(PLATFORM, platform);
    }

    private static void setDefaultValueForSystemProperty(String propName, Properties propertyFile) {
        setDefaultValueForSystemProperty(propName, propertyFile.getProperty(propName));
    }

    private static String getPropertyValue(String propertyName) {
        return System.getProperty(propertyName);
    }

    public static void setDefaultValueForAppiumServiceUrl(String url) {
        setDefaultValueForSystemProperty(APPIUM_SERVICE_URL, url);
    }

    public static URL getAppiumServiceUrl() {
        URL url = null;
        try {
            url = new URL(getPropertyValue(APPIUM_SERVICE_URL));
        } catch (MalformedURLException e) {
            CustomAssert.failTest("APPIUM SERVER URL NOT SET " + e.getMessage());
        }
        return url;
    }

    public static String getBaseUiUrl() {
        return getPropertyValue(BASE_UI_URL);
    }

    public static String getBaseApiUrl() {
        return getPropertyValue(BASE_API_URL);
    }

    public static String getDriver() {
        return getPropertyValue(DRIVER);
    }

    public static String getPlatform() {
        return getPropertyValue(PLATFORM);
    }

    public static String getDbUrl() {
        return getPropertyValue(DB_URL);
    }

    public static String getDbUser() {
        return getPropertyValue(DB_USER);
    }

    public static String getDbPassword() {
        return getPropertyValue(DB_PASSWORD);
    }

    public static boolean isHeadless() {
        return GraphicsEnvironment.isHeadless();
    }

    public static int getTimeout() {
        return Integer.parseInt(getPropertyValue(TIMEOUT));
    }

    public static List<String> getIncludeGroups() {
        if (System.getProperties().containsKey(INCLUDE_GROUPS)) {
            return Arrays.asList(getPropertyValue(INCLUDE_GROUPS).split("/"));
        }
        return new ArrayList<>();
    }

    public static List<String> getExcludeGroups() {
        if (System.getProperties().containsKey(EXCLUDE_GROUPS)) {
            return Arrays.asList(getPropertyValue(EXCLUDE_GROUPS).split("/"));
        }
        return new ArrayList<>();
    }
}
