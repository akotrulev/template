package utility;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Every page object will inherit this class, giving it access to it's methods. First PO class initialisation
 * the Webdriver will be parsed via constructor
 */
public class BasePageObject {
    protected WebDriver webDriver;
    protected WebDriverWait webDriverWait;
    private String baseUrl;

    public BasePageObject(WebDriver webDriver) {
        baseUrl = System.getProperty(SystemPropertyUtil.BASE_UI_URL);
        this.webDriver = webDriver;
        SystemPropertyUtil.loadAllPropsFromFiles();
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(SystemPropertyUtil.getTimeout()), Duration.ofMillis(50));
    }

    public void goToBasePage() {
        Reporter.log("Opening home page " + baseUrl);
        webDriver.get(baseUrl);
    }

    public void navigateToUrl(String url) {
        Reporter.log("Opening page " + url);
        webDriver.get(url);
    }

    protected String getElementText(WebElement element) {
        Reporter.log("Getting text from element " + element.getTagName() + "and trimming white spaces");
        return element.getText().trim();
    }

    protected String getElementText(By locator) {
        return getElementText(findElementByLocator(locator));
    }

    protected void clearField(WebElement element) {
        int length = element.getAttribute("value").length();
        for (int i = 0; i < length; i++) {
            element.sendKeys(Keys.BACK_SPACE);
        }
    }

    protected void writeText(WebElement element, String text) {
        Reporter.log("Clearing text from " + element.getTagName());
        clearField(element);
        ExpectedCondition<Boolean> elementTextEqualsString = arg0 -> element.getText().equals("");
        try {
            webDriverWait.until(elementTextEqualsString);
        } catch (TimeoutException e) {
            CustomAssert.failTest(String.format("Failed on waiting for element '%s' text to equal '%s'.", element, text));
        }
        Reporter.log("Writing text " + text + " to element " + element.getTagName());
        element.sendKeys(text);
    }

    protected void writeText(By locator, String text) {
        writeText(findElementByLocator(locator), text);
    }

    protected void clickElement(WebElement element) {
        Reporter.log("Clicking element " + element.getTagName());
        element.click();
    }

    protected void clickElement(By locator) {

        Reporter.log("Waiting for element to be clickable " + locator.toString());
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
        } catch (TimeoutException e) {
            CustomAssert.failTest(String.format("Failed on waiting for element '%s' to be clickable.", locator));
        }

        clickElement(findElementByLocator(locator));
    }

    public void quitDriver() {
        webDriver.quit();
    }

    protected WebElement findElementByLocator(By locator) {
        waitForElementWithLocator(locator);
        return webDriver.findElement(locator);
    }

    protected List<WebElement> findElementsByLocator(By locator) {
        waitForElementWithLocator(locator);
        return webDriver.findElements(locator);
    }

    protected void waitForElementWithLocator(By locator) {
        Reporter.log("Waiting for presence of element " + locator.toString());
        try {
            webDriverWait.until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (TimeoutException e) {
            CustomAssert.failTest(String.format("Failed on waiting for element '%s' to be present.", locator));
        }

        Reporter.log("Waiting for visibility of element " + locator);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            CustomAssert.failTest(String.format("Failed on waiting for element '%s' to be visible.", locator));
        }

    }

    protected boolean isElementVisible(By locator) {
        try {
            Reporter.log("Waiting for visibility of element " + locator.toString());
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    protected boolean isElementInvisible(By locator) {
        try {
            Reporter.log("Waiting for invisibility of element " + locator.toString());
            webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    protected void waitForElementToDisappear(By locator) {
        Reporter.log("Waiting for invisibility of element " + locator.toString());
        try {
            webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            CustomAssert.failTest(String.format("Failed on waiting for element '%s' to disappear.", locator));
        }
    }

    public void addCookieToBrowser(Cookie cookie) {
        webDriver.manage().addCookie(cookie);
        webDriver.navigate().refresh();
    }

    public void addCookieToBrowser(List<Cookie> cookies) {
        cookies.forEach(cookie -> webDriver.manage().addCookie(cookie));
        webDriver.navigate().refresh();
    }

    public void scrollDown(int deltaY) {
        new Actions(webDriver).scrollByAmount(0, deltaY).build().perform();
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void submitForm(By locator) {
        Reporter.log("Submit form " + locator.toString());
        findElementByLocator(locator).submit();
    }

    public void hoverOverElementByLocator(By locator) {
        Reporter.log("Hover over " + locator.toString());
        WebElement element = findElementByLocator(locator);
        hoverOverElement(element);
    }

    public void hoverOverElement(WebElement element) {
        Actions action = new Actions(webDriver);
        action.moveToElement(element).build().perform();
    }

    public void mobileScrollToElement(By locator) {
        if (SystemPropertyUtil.getDriver().equals(BrowserName.ANDROID)) {
            new Actions(webDriver)
                    .scrollToElement(findElementByLocator(locator))
                    .perform();
        } else {
            //TODO we might want to replace this with Sequence as Actions doesn't seem to be supported by safari
            webDriver.navigate().refresh();
            JavascriptExecutor js = (JavascriptExecutor) webDriver;
            Map<String, Object> params = new HashMap<>();
            params.put("direction", "down");
            js.executeScript("mobile: scroll", params);
        }
    }

    public void clearField(By locator) {
        Reporter.log("Clear field with locator " + locator.toString());
        findElementByLocator(locator).clear();
    }

    public void selectMenuItem(By locator, String menuItem) {
        Select select = new Select(findElementByLocator(locator));
        select.selectByVisibleText(menuItem);
    }

    public void switchToIFrameByLocator(By iframe) {
        webDriver.switchTo().frame(findElementByLocator(iframe));
    }

    public void switchToWindowsHandler(String windowsHandler) {
        Reporter.log("Switching to window " + windowsHandler);
        webDriver.switchTo().window(windowsHandler);
    }

    public void switchToDefaultContent() {
        webDriver.switchTo().defaultContent();
    }

    public void waitUntilUrlChange() {
        String url = webDriver.getCurrentUrl();
        waitUntilUrlChange(url);
    }

    public void waitUntilUrlChange(String url) {
        webDriverWait.until(ExpectedConditions.not(ExpectedConditions.urlToBe(url)));
    }
}

