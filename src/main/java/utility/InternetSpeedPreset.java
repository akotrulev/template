package utility;

import org.openqa.selenium.chromium.ChromiumNetworkConditions;

import java.time.Duration;

public class InternetSpeedPreset {
    private static ChromiumNetworkConditions InternetSpeedPreset(boolean isOffline, int latencyInMilliseconds, int downloadLimitKbps, int uploadLimitKbps) {
        ChromiumNetworkConditions chromiumNetworkConditions = new ChromiumNetworkConditions();
        chromiumNetworkConditions.setOffline(isOffline);
        chromiumNetworkConditions.setLatency(Duration.ofMillis(latencyInMilliseconds));
        chromiumNetworkConditions.setDownloadThroughput(downloadLimitKbps);
        chromiumNetworkConditions.setUploadThroughput(uploadLimitKbps);
        return chromiumNetworkConditions;
    }

    public final static ChromiumNetworkConditions OFFLINE = InternetSpeedPreset(true, 0, 0, 0);

    public final static ChromiumNetworkConditions SLOW3G= InternetSpeedPreset(false,2000,51200,51200);

    public final static ChromiumNetworkConditions FAST3G = InternetSpeedPreset(false, 550, 188743,  94371);
    public final static ChromiumNetworkConditions NO_THROTTLE = InternetSpeedPreset(false, 0, 5000000,  5000000);

//GPRS,50,20,500
//Regular 2G,250,50,300
//Good 2G,450,150,150
//Regular 3G,750,250,100
//Good 3G, 1000,750,40
//Regular 4G, 4000,3000,20
//DSL 2000, 1000,5
//WiFi 30000,15000,2
}
