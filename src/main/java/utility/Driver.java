package utility;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;

import java.util.logging.Level;
import java.util.logging.Logger;

import static java.awt.GraphicsEnvironment.isHeadless;
import static utility.BrowserName.*;

public class Driver {
    protected static final ThreadLocal<WebDriver> threadDriver = new ThreadLocal<>();

    public static WebDriver getDriver() {
        return threadDriver.get();
    }

    public WebDriver startNewDriver() {
        Logger.getLogger("org.openqa.selenium").setLevel(Level.SEVERE);
        String driverType = SystemPropertyUtil.getDriver().toLowerCase();
        WebDriver webDriver = null;
        switch (driverType.toLowerCase()) {
            case CHROME:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.WEB);
                WebDriverManager.chromedriver().setup();
                if (isHeadless()) {
                    ChromeOptions options = new ChromeOptions();
                    options.setHeadless(true)
                            .addArguments("--disable-gpu",
                                    "--window-size=1920,1200",
                                    "--ignore-certificate-errors",
                                    "--disable-extensions",
                                    "--no-sandbox",
                                    "--disable-dev-shm-usage",
                                    "--verbose");
                    webDriver = new ChromeDriver(options);
                } else {
                    webDriver = new ChromeDriver();
                    webDriver.manage().window().setSize(new Dimension(1920, 1080));
                }

            case FIREFOX:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.WEB);
                WebDriverManager.firefoxdriver().setup();
                if (isHeadless()) {
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    firefoxOptions.setHeadless(true);
                    webDriver = new FirefoxDriver(firefoxOptions);
                } else
                    webDriver = new FirefoxDriver();

            case EDGE:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.WEB);
                WebDriverManager.edgedriver().setup();
                if (isHeadless()) {
                    EdgeOptions edgeOptions = new EdgeOptions();
                    edgeOptions.setHeadless(true);
                    webDriver = new EdgeDriver(edgeOptions);
                } else
                    webDriver = new EdgeDriver();

            case SAFARI:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.WEB);
                WebDriverManager.safaridriver().setup();
                //No headless available for safari as per https://github.com/SeleniumHQ/selenium/issues/5985
                webDriver = new SafariDriver();
                webDriver.manage().window().maximize();

            case ANDROID:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.M_WEB);
                webDriver = DeviceFactory.getAndroidDevice();

            case IPHONE:
            case IPAD:
                SystemPropertyUtil.setDefaultValueForPlatform(PlatformName.M_WEB);
                webDriver = DeviceFactory.getIosDevice();
        }
        if (webDriver == null) {
            throw new IllegalStateException("Error while creating driver: " + driverType);
        }
        threadDriver.set(webDriver);
        return webDriver;
    }
}

