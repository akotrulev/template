package utility;

public class BrowserName {
    public final static String FIREFOX = "firefox";
    public final static String SAFARI = "safari";
    public final static String EDGE = "edge";
    public final static String CHROME = "chrome";
    public final static String ANDROID = "android";
    public final static String IPHONE = "iphone";
    public final static String IPAD = "ipad";
}
